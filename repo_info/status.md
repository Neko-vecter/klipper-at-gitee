# Sync Status

这是项目维护通知页面

### 2024-10-01
迁移同步到虚拟化集群提升可靠性

### 2024-09-01
由于同步组件架构更改中。暂时暂停同步以下组件。

预计恢复时间：2024-09-15

实际恢复时间：2024-09-08

#### 受影响的组件
- Happy-Hare
- mainsail-config
- fluidd-config
- Mainsail（webui）
- Fluidd（webui）
- Moonraker Timelapse
- KlipperScreen
- klipper-led_effect
