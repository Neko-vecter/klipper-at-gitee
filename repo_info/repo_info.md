# 仓库维护信息

### Klipper 安装

#### Kiauh（Klipper 安装助手）

项目地址：[gitee.com/miroky/kiauh](https://gitee.com/miroky/kiauh)

项目维护：[@miroky](https://gitee.com/miroky)

--- 

### Klipper 组件

#### Klipper

项目地址：[gitee.com/miroky/klipper](https://gitee.com/miroky/klipper)

项目维护：[@miroky](https://gitee.com/miroky)

#### Moonraker

项目地址：[gitee.com/miroky/moonraker](https://gitee.com/miroky/moonraker)

项目维护：[@miroky](https://gitee.com/miroky) [@Neko-vecter](https://gitee.com/Neko-vecter)

#### Mainsail（webui）

项目地址：[gitee.com/Neko-vecter/mainsail-releases](https://gitee.com/Neko-vecter/mainsail-releases)

项目维护：[@Neko-vecter](https://gitee.com/Neko-vecter)

#### Fluidd（webui）

项目地址：[gitee.com/Neko-vecter/fluidd-releases](https://gitee.com/Neko-vecter/fluidd-releases)

项目维护：[@Neko-vecter](https://gitee.com/Neko-vecter)

---

### Moonraker 组件

#### Moonraker Timelapse

项目地址：[gitee.com/Neko-vecter/moonraker-timelapse](https://gitee.com/Neko-vecter/moonraker-timelapse)

项目维护：[@Neko-vecter](https://gitee.com/Neko-vecter)

#### Crowsnest

项目地址：[gitee.com/Neko-vecter/crowsnest](https://gitee.com/Neko-vecter/crowsnest)

项目维护：[@miroky](https://gitee.com/miroky) [@Neko-vecter](https://gitee.com/Neko-vecter)

#### camera-streamer

项目地址：[gitee.com/Neko-vecter/camera-streamer](https://gitee.com/Neko-vecter/camera-streamer)

项目维护：[@Neko-vecter](https://gitee.com/Neko-vecter)

#### KlipperScreen

项目地址：[gitee.com/Neko-vecter/KlipperScreen](https://gitee.com/Neko-vecter/KlipperScreen)

项目维护：[@Neko-vecter](https://gitee.com/Neko-vecter)

#### Happy-Hare

项目地址：[gitee.com/Neko-vecter/Happy-Hare](https://gitee.com/Neko-vecter/Happy-Hare)

项目维护：[@Neko-vecter](https://gitee.com/Neko-vecter)

#### klipper-led_effect

项目地址：[gitee.com/Neko-vecter/klipper-led_effect](https://gitee.com/Neko-vecter/klipper-led_effect)

项目维护：[@Neko-vecter](https://gitee.com/Neko-vecter)

---

### Klipper Config file

#### mainsail-config

项目地址：[gitee.com/Neko-vecter/mainsail-config](https://gitee.com/Neko-vecter/mainsail-config)

项目维护：[@Neko-vecter](https://gitee.com/Neko-vecter)

#### fluidd-config

项目地址：[gitee.com/Neko-vecter/fluidd-config](https://gitee.com/Neko-vecter/fluidd-config)

项目维护：[@Neko-vecter](https://gitee.com/Neko-vecter)
