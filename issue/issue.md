# Issue 应该包含的内容

### Issue Checklist
- 遇到的问题
  - 详细描述问题
  - 尝试过的解决方案（例如更换设备
  - 可能解决问题的方案

- 预期结果
- 实际结果

- 遇到问题的报错截图
- 使用的网络环境
- neofetch 截图

### 如何安装neofetch？
#### Debain
``` shell
sudo apt-get install neofetch
```

#### Arch (Pacman)
``` shell
sudo pacman -S neofetch
```

