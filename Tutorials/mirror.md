# 更改仓库的更新源

### 更改 klipper 仓库源地址

``` shell
sudo service klipper stop
cd ~/klipper
git remote set-url origin https://gitee.com/miroky/klipper.git
git pull
sudo service klipper start
```

### 更改moonraker 仓库源地址

``` shell
sudo service moonraker stop
cd ~/moonraker
git remote set-url origin https://gitee.com/miroky/moonraker.git
git pull
sudo service moonraker start
```

### Fluidd/ Mainsail
已经集成在 `Moonraker` 里面了，更新 `Moonraker` 仓库地址即可~

### 更改mainsail config
``` shell
sudo service klipper stop
cd ~/mainsail-config
git remote set-url origin https://gitee.com/Neko-vecter/mainsail-config.git
git pull
sudo service klipper start
```

然后在 `moonraker.conf` 中更改 `[update_manager mainsail-config]`
```
[update_manager mainsail-config]
type: git_repo
primary_branch: master
path: ~/mainsail-config
origin: https://gitee.com/Neko-vecter/mainsail-config.git
managed_services: klipper 
```

### fluidd config
``` shell
sudo service klipper stop
cd ~/mainsail-config
git remote set-url origin https://gitee.com/Neko-vecter/fluidd-config.git
git pull
sudo service klipper start
```

然后在 `moonraker.conf` 中更改 `[update_manager fluidd-config]`
```
[update_manager fluidd-config]
type: git_repo
primary_branch: master
path: ~/fluidd-config
origin: https://gitee.com/Neko-vecter/fluidd-config.git
managed_services: klipper
```

### ToDo List
- ~~全自动化换源工具~~
