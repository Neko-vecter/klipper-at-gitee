# 自动更新源工具使用

### Clone 仓库
``` shell
cd ~
git clone https://gitee.com/Neko-vecter/klipper-at-gitee.git
```

### 安装使用
``` shell
cd ~
bash ~/klipper-at-gitee/tools/mirror-change.sh
```

### 更换系统源
参考 [tuna的教程](https://mirrors.tuna.tsinghua.edu.cn/help/debian/)

### 更换pypi mirror
``` shell
cd ~
bash ~/klipper-at-gitee/tools/pypi-mirror.sh
```