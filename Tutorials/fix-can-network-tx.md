# 配置Linux CAN Network/修复CAN Network 缓冲区大小不正确

修改`/etc/network/interfaces.d/can0`

```
sudo nano /etc/network/interfaces.d/can0
```

添加以下配置

``` shell
allow-hotplug can0
iface can0 can static
    bitrate 1000000
    up ip link set can0 txqueuelen 1000
```

然后使用以下命令重启

``` shell
sudo systemctl restart networking
```
